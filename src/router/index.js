import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import TodayPage from '../views/TodayPage.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/today',
    name: 'Today',
    component: TodayPage
  }
]

const router = new VueRouter({
  routes
})

export default router
